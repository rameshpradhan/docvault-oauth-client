import os

from flask import Flask, redirect, request, session, url_for
from flask.json import jsonify
from requests_oauthlib import OAuth2Session

from settings import (
    KEYCLOAK_AUTHORIZATION_BASE_URL,
    KEYCLOAK_CLIENT_ID,
    KEYCLOAK_CLIENT_SECRET,
    KEYCLOAK_TOKEN_URL,
    REDIRECT_URI,
)

app = Flask(__name__)

# these scopes is optional. All scopes listed below must also be listed in client scopes defined in Keycloak
scope = [
    "media:create",
    "media:read",
    "media:update",
    "media:delete",
    "project:delete",
    "project:read",
]


@app.route("/")
def home():
    """
    Step 1: User Authorization.

    Redirect the user/resource owner to the OAuth provider (i.e. Keycloak Auth Server)
    using an URL with a few key OAuth parameters.
    """
    docvault = OAuth2Session(
        KEYCLOAK_CLIENT_ID,
        redirect_uri=REDIRECT_URI,
        scope=",".join(scope),  # scope is optional
    )
    authorization_url, state = docvault.authorization_url(
        KEYCLOAK_AUTHORIZATION_BASE_URL,
    )
    print(authorization_url)
    # State is used to prevent CSRF, keep this for later.
    session["oauth_state"] = state
    return redirect(authorization_url)


# Step 2: User authorization, this happens on the provider.
@app.route("/callback", methods=["GET"])
def callback():
    """
    Step 3: Retrieving an access token.

    The user has been redirected back from the provider to your registered
    callback URL. With this redirection comes an authorization code included
    in the redirect URL. We will use that to obtain an access token.
    """
    docvault = OAuth2Session(
        KEYCLOAK_CLIENT_ID, state=session["oauth_state"], redirect_uri=REDIRECT_URI
    )
    code = request.args.get("code")
    token = docvault.fetch_token(
        KEYCLOAK_TOKEN_URL,
        client_secret=KEYCLOAK_CLIENT_SECRET,
        code=code,
        include_client_id=True,
    )
    # lets save the token for future use
    session["oauth_token"] = token
    return redirect(url_for(".access_token"))


@app.route("/access-token", methods=["GET"])
def access_token():
    return jsonify(session["oauth_token"])


@app.route("/projects", methods=["GET"])
def project():
    """Fetching a protected resource using an OAuth 2 token."""
    import requests

    url = "https://docvault.crecentric.com/api/projects/?page=1"

    payload = {}

    headers = {
        "Authorization": session["oauth_token"]["access_token"],
        "Content-Type": "application/x-www-form-urlencoded",
        "x-token-type": "Cross-Web",
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    return response.json()


if __name__ == "__main__":
    # This allows us to use a plain HTTP callback
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    app.secret_key = os.urandom(24)
    app.run(debug=True)
