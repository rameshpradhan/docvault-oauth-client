# Docvault OAuth Quickstart tutorial

## A top-level directory layout

    .
    ├── templates/                               # All html templates
    ├── venv/                                    # Python Virtual environment
    ├── oauth_client_docvault.py                 # main python script file
    ├── settings.py                              # contains environment variables
    ├── requirements.txt                         # dependencies
    └── README.md                                # documentation
This guide will walk-through with python based OAuth client: [requests-oauthlib](https://pypi.org/project/requests-oauthlib/). Please make sure that you have python3 and pip3 installed in your machine.

## Create virtual environment

```shell
virtualenv -p /usr/bin/python3 venv
source ./venv/bin/activate
pip install -r requirements.txt
```

## Create new file <oauth_client_docvault.py>

### Imports: Import all requirements

```python
import os

from flask import Flask, redirect, request, session, url_for
from flask.json import jsonify
from requests_oauthlib import OAuth2Session

from settings import (
    KEYCLOAK_AUTHORIZATION_BASE_URL,
    KEYCLOAK_CLIENT_ID,
    KEYCLOAK_CLIENT_SECRET,
    KEYCLOAK_TOKEN_URL,
    REDIRECT_URI,
)

app = Flask(__name__)


scope = [
    "media:create",
    "media:read",
    "media:update",
    "media:delete",
    "project:delete",
    "project:read",
]
```

### Step 1: Creating home route for User Authorization

```python
@app.route("/")
def home():
    """
    Step 1: User Authorization.

    Redirect the user/resource owner to the OAuth provider (i.e. Keycloak Auth Server)
    using an URL with a few key OAuth parameters.
    """
    docvault = OAuth2Session(
        KEYCLOAK_CLIENT_ID,
        redirect_uri=REDIRECT_URI,
        scope=",".join(scope),  # scope is optional
    )
    authorization_url, state = docvault.authorization_url(
        KEYCLOAK_AUTHORIZATION_BASE_URL,
    )
    print(authorization_url)
    # State is used to prevent CSRF, keep this for later.
    session["oauth_state"] = state
    return redirect(authorization_url)
```

### Step 2: User Authorization

```python
# Step 2: User authorization, this happens on the provider. This is processed by Keycloak Auth Server.
```

### Step 3: Retrieving an access token

```python
@app.route("/callback", methods=["GET"])
def callback():
    """
    Step 3: Retrieving an access token.

    The user has been redirected back from the provider to your registered
    callback URL. With this redirection comes an authorization code included
    in the redirect URL. We will use that to obtain an access token.
    """
    docvault = OAuth2Session(
        KEYCLOAK_CLIENT_ID, state=session["oauth_state"], redirect_uri=REDIRECT_URI
    )
    code = request.args.get("code")
    token = docvault.fetch_token(
        KEYCLOAK_TOKEN_URL,
        client_secret=KEYCLOAK_CLIENT_SECRET,
        code=code,
        include_client_id=True,
    )
    # lets save the token for future use
    session["oauth_token"] = token
    return redirect(url_for(".access_token"))
```

### Step 4: Displaying OAuth token to the frontend

```python
@app.route("/access-token", methods=["GET"])
def access_token():
    return jsonify(session["oauth_token"])
```

### Step 5: Example of Accessing API endpoint of <https://docvault.crecentric.com/>

```python
@app.route("/projects", methods=["GET"])
def project():
    """Fetching a protected resource using an OAuth 2 token."""
    import requests

    url = "https://docvault.crecentric.com/api/projects/?page=1"

    payload = {}

    headers = {
        "Authorization": session["oauth_token"]["access_token"],
        "Content-Type": "application/x-www-form-urlencoded",
        "x-token-type": "Cross-Web",
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    return response.json()
```

### Step 6: Running the flask app

```python
if __name__ == "__main__":
    # This allows us to use a plain HTTP callback
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    app.secret_key = os.urandom(24)
    app.run(debug=True)

```

## Create new file: <settings.py> Please make sure to replace all listed variable from Keycloak server

```python
KEYCLOAK_URL = "https://docvault.crecentric.com/auth/"
KEYCLOAK_REALM_NAME = "docvault"
KEYCLOAK_CLIENT_ID = <YOUR_CLIENT_ID>
KEYCLOAK_CLIENT_SECRET = <YOUR_CLIENT_SECRET>
REDIRECT_URI = "http://localhost:5000/callback"
KEYCLOAK_AUTHORIZATION_BASE_URL = "https://docvault.crecentric.com/auth/realms/docvault/protocol/openid-connect/auth"
KEYCLOAK_TOKEN_URL = "https://docvault.crecentric.com/auth/realms/docvault/protocol/openid-connect/token"
```

## To start application server

```shell
python oauth_client_docvault.py
```

## Process to communicate with docvault

* Visit <http://localhost:5000>
* App will be redirected to Keycloak Auth Server
* Enter credential
* Keycloak will redirect user to our Flask APP to url: <http://localhost:5000/access-token>
* Visit <http://localhost:5000/projects>
