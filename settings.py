KEYCLOAK_URL = "https://docvault.crecentric.com/auth/"
KEYCLOAK_REALM_NAME = "docvault"
KEYCLOAK_CLIENT_ID = "<YOUR_CLIENT_ID>"
KEYCLOAK_CLIENT_SECRET = "<YOUR_CLIENT_SECRET"
KEYCLOAK_AUTHORIZATION_BASE_URL = (
    "https://docvault.crecentric.com/auth/realms/docvault/protocol/openid-connect/auth"
)
KEYCLOAK_TOKEN_URL = (
    "https://docvault.crecentric.com/auth/realms/docvault/protocol/openid-connect/token"
)
REDIRECT_URI = "http://localhost:5000/callback"